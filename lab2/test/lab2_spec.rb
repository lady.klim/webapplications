require 'rspec'
require_relative '../lib/lab1.rb'

describe "print greeting method" do
  it "should print for age >18" do
    expect(STDOUT).to receive(:puts).with("Привет, Светлана Клим. Самое время заняться делом!")
    print_greeting("Светлана", "Клим", 19)
  end
  it "should print for age <18" do
    expect(STDOUT).to receive(:puts).with("Привет, Светлана Клим. Тебе меньше 18 лет, но начать учиться программировать никогда не рано")
    print_greeting("Светлана", "Клим", 17)
  end
  it "should throw TypeError" do
    expect { print_greeting("Светлана", "Клим", "not an Integer") }.to raise_error(TypeError)
  end
end

describe "foobar func" do
  it "should return 10" do
    expect(foobar(20, 10)).to eq(10)
  end
  it "should return 20" do
    expect(foobar(10, 10)).to eq(20)
  end
  it "should throw TypeError" do
    expect { foobar("20", 10) }.to raise_error(TypeError)
  end
end

